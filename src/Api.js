import axios from 'axios'

axios.defaults.baseURL = process.env.BASE_URL + "api"

export default {
  createConfig (query) {
    let config = {
      headers: {
        "Authorization": "Bearer " + localStorage.getItem("token"),
      },
      query
    }

    return config
  },
  postLogin (query) {
    return axios.post('/auth/login', query)
  },
  getSongs (query) {
    return axios.get('/search', {params: query})
  },
  getNumSongs (query) {
    return axios.get('/songsnum', {params: query})
  },
  getAllSongs () {
    return axios.get('/songs/')
  },
  getSong (id) {
    return axios.get('/song/' + id)
  },
  searchSongs (query) { 
    return axios.post('/search/', {body: query})
  },
  uploadFile (query) {
    return axios.post('/upload-file', this.createConfig (query))
  },
  updateFile (data) {
    return axios.post('/update-file', this.createConfig (data))
  },
  deleteFile (data) {
    return axios.post('/delete-file', this.createConfig (data))
  },
  addSong (data) {
    return axios.post('/add-song', this.createConfig (data))
  },
  editSong (data) {
    return axios.post('/update-song', this.createConfig (data))
  },
  deleteSong (data) {
    return axios.post('/delete-song', this.createConfig (data))
  },
  sendChange(data) {
      return axios.post('/save-log', this.createConfig (data))
  },
  getLogs() {
    let config = {
      headers: {
        "Authorization": "Bearer " + localStorage.getItem("token"),
      }
    }
    return axios.get('/logs', config)
  },
  getUsers() {
    let config = {
      headers: {
        "Authorization": "Bearer " + localStorage.getItem("token"),
      }
    }
    return axios.get('/users', config)
  },
  updateAuthor(data) {
    return axios.post('/update-author', this.createConfig (data))
  },
  sendInvitation(data) {
    return axios.post('/send-invitation', this.createConfig(data))
  },
  removeUser(data) {
    return axios.post('/remove-user', this.createConfig(data))
  },
  changeMainAdminStatus(data) {
    return axios.post('/main-admin-status', this.createConfig(data))
  },
  registerUser(data) {
    return axios.post('/register', this.createConfig(data))
  }
}
