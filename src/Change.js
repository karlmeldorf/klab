export default class Change {
    constructor(songTitles, changeText, author, date, songID, field) {
        this.songTitles = songTitles,
            this.changeText = changeText,
            this.author = author,
            this.date = date,
            this.songID = songID,
            this.field = field
    }
}