import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'
import Search from '@/views/Search.vue'
import AddSong from '@/views/AddSong.vue'
import Login from '@/views/Login.vue'
import EditSong from '@/views/EditSong.vue'
import DetailView from '@/views/DetailView.vue'
import Err404 from '@/views/404.vue'
import Log from '@/views/Log.vue'
import AdminPanel from '@/views/AdminPanel.vue'
import AccessDenied from '@/views/AccessDenied.vue'
import Register from '@/views/Register.vue'

Vue.use(Router)

let router = new Router({
    mode: 'history',
    routes: [
        { path: '/', component: Home },
        { path: '/search', component: Search },
        { path: '/results', component: Search },
        {
            path: '/editSong/:songId',
            name: 'editSong',
            component: EditSong,
            meta: {
                requiresAuth: true
            }

        },
        {
            path: '/lisa-laul',
            name: 'add-song',
            component: AddSong,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/log',
            name: 'log',
            component: Log,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/admin-panel',
            name: 'admin-panel',
            component: AdminPanel,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/admin',
            name: 'Login',
            component: Login
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        { path: '/detailView/:songId', name: 'detailView', component: DetailView },
        { path: '*', component: Err404 },
        { path: '/accessDenied', component: AccessDenied }
    ]
})

export default router