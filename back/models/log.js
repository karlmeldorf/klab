const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const logSchema = new Schema({
    songTitles: {type: String},
    changeText: {type: String},
    author: {type: String},
    date: {type: String},
    songID: {type: String}
    

})

module.exports = mongoose.model('Log', logSchema);