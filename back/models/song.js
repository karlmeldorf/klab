const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const autoIncrementModelID = require('./counter');

const personDataSchema = new Schema({
    firstName: { type: String, trim: true, minlength: 1 },
    lastName: { type: String, trim: true, minlength: 1 },
    birthDate: { type: String, trim: true, minlength: 1 },
    deathDate: { type: String, trim: true, minlength: 1 },
})

// Parallel translation and it's author Schema.
// This Schema is used in songSchema inside list "parallelTranslations"
const parallelTranslationSchema = new Schema({
    translationAuthor: { type: personDataSchema },
    translationText: { type: String, trim: true, minlength: 1 },
    translationTextOrigin: { type: String, trim: true, minlength: 1 }
})

//Schema for recording of the song
//This schema is used in songSchema inside list "recordings"
const fileAndDescriptionSchema = new Schema({
    description: { type: String, trim: true, minlength: 1 },
    fileLocation: { type: String, trim: true, minlength: 1 }
})

const referenceSchema = new Schema({
    referenceString: { type: String, trim: true, minlength: 1 }
})

//Song's data model - consists all information about the song.
const songSchema = new Schema({
    UID: { type: Number },

    title: {
        estNorth: {type: String, trim: true, minlength: 1, 
            required: function(){return this.title.estSouth === undefined && this.title.original === undefined;}
            },
        estSouth: {type: String, trim: true, minlength: 1,
            required: function(){return this.title.estNorth === undefined && this.title.original === undefined;}
            },
        original:  {type: String, trim: true, minlength: 1,
            required: function(){return this.title.estNorth === undefined && this.title.estSouth === undefined;}
            }
    },

    textAuthor: {
        type: personDataSchema
    },

    originalText: { type: String, trim: true, minlength: 1 },
    textOrigin: { type: String, trim: true, minlength: 1 },

    melodyAuthor: {
        type: personDataSchema,
    },

    melodyInfo: { type: String, trim: true, minlength: 1 },
    melodyOrigin: { type: String, trim: true, minlength: 1 },

    translationAuthor: {
        type: personDataSchema
    },

    translationText: { type: String, trim: true, minlength: 1 },
    translationTextOrigin: { type: String, trim: true, minlength: 1 },

    parallelTranslations: { type: [parallelTranslationSchema] },

    rhythm: { type: String, trim: true, minlength: 1 },
    strroof: { type: String, trim: true, minlength: 1 },

    recordings: { type: [fileAndDescriptionSchema] },
    pdfs: { type: [fileAndDescriptionSchema] },
    pdfLocation: { type: String, trim: true, minlength: 1 },
    xmlLocation: { type: String, trim: true, minlength: 1 },

    copyRights: {
        melody: { type: String, trim: true, minlength: 1 },
        text: { type: String, trim: true, minlength: 1 },
        translation: { type: String, trim: true, minlength: 1 },
        publicUse: { type: String, trim: true, minlength: 1 },
        publicize: { type: String, trim: true, minlength: 1 },
        arrangement: { type: String, trim: true, minlength: 1 },
        other: { type: String, trim: true, minlength: 1 }
    },

    bookReferences: {
        LR1721: { type: String, trim: true, minlength: 1 },
        LR1848: { type: String, trim: true, minlength: 1 },
        LR1899: { type: String, trim: true, minlength: 1 },
        KLPR1991: { type: String, trim: true, minlength: 1 },
        Punchel: { type: String, trim: true, minlength: 1 },
        Hagen1: { type: String, trim: true, minlength: 1 },
        Hagen2: { type: String, trim: true, minlength: 1 },
        Hagen3: { type: String, trim: true, minlength: 1 },
        UKLPR: { type: String, trim: true, minlength: 1 },
        KreegiRKVfond: { type: String, trim: true, minlength: 1 }
    },

    bookReferencesPDF: {
        LR1721: { type: String, trim: true, minlength: 1 },
        LR1848: { type: String, trim: true, minlength: 1 },
        LR1899: { type: String, trim: true, minlength: 1 },
        KLPR1991: { type: String, trim: true, minlength: 1 },
        Punchel: { type: String, trim: true, minlength: 1 },
        Hagen1: { type: String, trim: true, minlength: 1 },
        Hagen2: { type: String, trim: true, minlength: 1 },
        Hagen3: { type: String, trim: true, minlength: 1 },
        UKLPR: { type: String, trim: true, minlength: 1 },
        KreegiRKVfond: { type: String, trim: true, minlength: 1 }
      },

    bibleReferences: { type: String, trim: true, minlength: 1 },
    humnInfo: { type: String, trim: true, minlength: 1 },

    chapterUKLPR: { type: String, trim: true, minlength: 1 },
    subchapterUKLPR: { type: String, trim: true, minlength: 1 },
    numberUKLPR: { type: String, trim: true, minlength: 1 },

    comments: { type: String, trim: true, minlength: 1 }
}, { timestamps: true });

songSchema.pre('save', function (next) {
    if (!this.isNew) {
      next();
      return;
    }
  
    autoIncrementModelID('song-UID', this, next);
});

module.exports = mongoose.model('Song', songSchema);