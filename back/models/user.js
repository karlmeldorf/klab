const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String },
    mainAdmin: {type: Boolean},
    firstname: { type: String },
    lastname: { type: String }
})

module.exports = mongoose.model('User', userSchema);