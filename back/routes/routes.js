const express = require('express');
const logController = require('../controllers/logController');
const searchController = require('../controllers/searchController');
const songController = require('../controllers/songController');
const userController = require('../controllers/userController');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

//POST uuenda autori andmed kõikides dokumentides
router.post('/update-author', isAuth, songController.postUpdateAuthor);

//POST lisa lugu
router.post('/add-song', isAuth, songController.postSong);

//POST uuenda lugu
router.post('/update-song', isAuth, songController.postUpdateSong);

//POST kustuta lugu
router.post('/delete-song', isAuth, songController.postDeleteSong);

//POST lae fail üles
router.post('/upload-file', isAuth, songController.postUploadFile);

//POST uuenda faili
router.post('/update-file', isAuth, songController.postUpdateFile);

//POST kustuta fail
router.post('/delete-file', isAuth, songController.postDeleteFile);

//GET kõik logid
router.get('/logs', isAuth, logController.getLogs);

//GET kõik laulud
router.post('/save-log', isAuth, logController.postLog);

//GET laulud query j2rgi uus
router.get('/search', searchController.getInitSongs);

//GET laulude arv
router.get('/songsnum', searchController.getNumSongs);

//GET kõik laulud
router.get('/songs', songController.getSongs);

//GET laul id järgi (e.g /song/5d8df57c362b3f0be0276329)
router.get('/song/:songId', songController.getSong);

//GET laulu PDF/XML pealkirja järgi (e.g /uploads/SchbAvMaSample.pdf)
//router.get('/mp3/uploads/:filename', indexController.getMp3); // streamiga, vaja veel korda teha, et saaks sliderit kasutada
router.get('/pdf/uploads/:filename', songController.getUploadedFile);

router.get('/xml/uploads/:filename', songController.getUploadedXmlFile);

router.get('/users', isAuth, userController.getUsers);

router.post('/send-invitation', isAuth, userController.sendInvitationEmail);

router.post('/remove-user', isAuth, userController.deleteUser);

router.post('/main-admin-status', isAuth, userController.changeMainAdminStatus);

router.post('/register', userController.registerUser);

module.exports = router;