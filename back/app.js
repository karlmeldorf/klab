//Third party packages
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const multer = require('multer');

//Meie enda koodi importid
const keys = require('./config');
const routes = require('./routes/routes');
const authRoutes = require('./routes/auth-routes');

const app = express();

//Vajalik middleware, et APId oleksid kättesaadavad
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

//faili hoiustamise kirjeldus
const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads');
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname.split('.').join('-' + Date.now() + '.'));
    }
});

//Kontrollime failitüüpi
const fileFilter = (req, file, cb) => {
    console.log(file.mimetype);
    if (file.mimetype === 'application/pdf' || file.mimetype === 'application/octet-stream' || file.mimetype === 'audio/mp3') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json());
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('file'));

app.use('/api', routes);
app.use('/api/auth', authRoutes);
// app.get('/', function(req, res) {
//     res.sendFile('/index.html', { root: __dirname });
// });

//error handeling
app.use((error, req, res, next) => {
    const status = error.statusCode || 500;
    const message = error.message;
    res.status(status).json({ message: message });
})

if (true) {
    app.use(express.static(__dirname + '/public'));

    app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'))
}

mongoose
    .connect(
        keys.mongodb.mongoSecret,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    )
    .then(result => {
        app.listen(5000);
        console.log("Server is up!")
    })
    .catch(err => {
        console.log(err)
    });