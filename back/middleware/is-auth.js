const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    let token = "";
    if (req.body.headers) {
        token = req.body.headers.Authorization.split(' ')[1];
    } else {
        token = req.headers.authorization.split(' ')[1];
    }
    let decodedToken;
    try {
        decodedToken = jwt.verify(token, 'karliheasecret')
    } catch (err) {
        err.statusCode = 500;
        throw err;
    }
    if (!decodedToken) {
        const error = new Error('Not authenticated.');
        error.statusCode = 401;
        throw error;
    }
    req.query.userId = decodedToken.userId;
    req.query.email = decodedToken.email;
    req.query.mainAdmin = decodedToken.mainAdmin;
    next();
};