const Song = require('../models/song');

exports.getNumSongs = (req, res, next) => {
    const error = new Error('Could not find a song.');
    error.statusCode = 404;

    const q = req.query.q;
    const r = req.query.rhythm;
    const b = req.query.book;
    let mongoQuery = {$and: [{}]};
    if (q !== undefined) {
        const queryParts = q.split("+");
        let critQuery = parseLogicalQuery(queryParts);
        mongoQuery.$and.push(critQuery);
    }
    if (r !== undefined) {
        mongoQuery.rhythm = r;
    }
    if (b !== undefined) {
        switch (b) {
            case 'Punschel':
                mongoQuery.$and.push({'bookReferences.Punchel': {$exists: true, $regex: '.+'}});
                break;
            case 'Hagen I':
                mongoQuery.$and.push({'bookReferences.Hagen1': {$exists: true, $regex: '.+'}});
                break;
            case 'Hagen II':
                mongoQuery.$and.push({'bookReferences.Hagen2': {$exists: true, $regex: '.+'}});
                break;
            case 'Hagen III':
                mongoQuery.$and.push({'bookReferences.Hagen3': {$exists: true, $regex: '.+'}});
                break;
            case 'LR1721':
                mongoQuery.$and.push({'bookReferences.LR1721': {$exists: true, $regex: '.+'}});
                break;
            case 'LR1848':
                mongoQuery.$and.push({'bookReferences.LR1848': {$exists: true, $regex: '.+'}});
                break;
            case 'LR1899':
                mongoQuery.$and.push({'bookReferences.LR1899': {$exists: true, $regex: '.+'}});
                break;
            case 'KLPR1991':
                mongoQuery.$and.push({'bookReferences.KLPR1991': {$exists: true, $regex: '.+'}});
                break;
            case 'UKLPR':
                mongoQuery.$and.push({'bookReferences.UKLPR': {$exists: true, $regex: '.+'}});
                break;
            case 'KreegiRKVfond':
                mongoQuery.$and.push({'bookReferences.KreegiRKVfond': {$exists: true, $regex: '.+'}});
                break;
            default:
                break;
        }
    }

    Song
    .find(mongoQuery)
    .countDocuments()
    .then(result => {
        res.status(200).json({ message: 'found', numSongs: result });
    })
    .catch(err => {
        const error = new Error('Not found. Invalid query.');
        error.statusCode = 401;
        console.log(err)
        return next(error);
    });
};

exports.getInitSongs = (req, res, next) => {
    const error = new Error('Could not find a song.');
    error.statusCode = 404;

    const start = parseInt(req.query.start);
    const q = req.query.q;
    const r = req.query.rhythm;
    const b = req.query.book;
    let mongoQuery = {$and: [{}]};
    if (q !== undefined) {
        const queryParts = q.split("+");
        let critQuery = parseLogicalQuery(queryParts);
        mongoQuery.$and.push(critQuery);
    }
    if (r !== undefined) {
        mongoQuery.$and.push({rhythm: r});
    }
    
    if (b !== undefined) {
        switch (b) {
            case 'Punschel':
                mongoQuery.$and.push({'bookReferences.Punchel': {$exists: true, $regex: '.+'}});
                break;
            case 'Hagen I':
                mongoQuery.$and.push({'bookReferences.Hagen1': {$exists: true, $regex: '.+'}});
                break;
            case 'Hagen II':
                mongoQuery.$and.push({'bookReferences.Hagen2': {$exists: true, $regex: '.+'}});
                break;
            case 'Hagen III':
                mongoQuery.$and.push({'bookReferences.Hagen3': {$exists: true, $regex: '.+'}});
                break;
            case 'LR1721':
                mongoQuery.$and.push({'bookReferences.LR1721': {$exists: true, $regex: '.+'}});
                break;
            case 'LR1848':
                mongoQuery.$and.push({'bookReferences.LR1848': {$exists: true, $regex: '.+'}});
                break;
            case 'LR1899':
                mongoQuery.$and.push({'bookReferences.LR1899': {$exists: true, $regex: '.+'}});
                break;
            case 'KLPR1991':
                mongoQuery.$and.push({'bookReferences.KLPR1991': {$exists: true, $regex: '.+'}});
                break;
            case 'UKLPR':
                mongoQuery.$and.push({'bookReferences.UKLPR': {$exists: true, $regex: '.+'}});
                break;
            case 'KreegiRKVfond':
                mongoQuery.$and.push({'bookReferences.KreegiRKVfond': {$exists: true, $regex: '.+'}});
                break;
            default:
                break;
        }
    }

    Song
    .find(mongoQuery)
    .limit(40)
    .skip(start)
    .then(result => {
        if (!result) {
            throw error;
        }
        res.status(200).json({ message: 'found', songs: result });
    })
    .catch(err => {
        const error = new Error('Not found. Invalid query.');
        error.statusCode = 401;
        return next(error);
    });

};

//helper function for parsing the query
function parseLogicalQuery(queryParts) {
    const searchExpression = constructLogExp(queryParts);
    let opStack = [];
    let valStack = [];

    searchExpression.forEach(element => {
        if (element === 'OR') {
            let topOp = opStack.pop();
            while (typeof topOp !== 'undefined') {
                const firstVal = valStack.pop();
                const secondVal = valStack.pop();
                let newVal = (topOp === 'OR') ? {$or: [firstVal, secondVal]} : {$and: [firstVal, secondVal]};
                valStack.push(newVal);
                topOp = opStack.pop();
            }
            opStack.push(element)
        } else if (element === 'AND') {
            let topOp = opStack.pop();
            while (typeof topOp !== 'undefined' && topOp === 'AND') {
                const firstVal = valStack.pop();
                const secondVal = valStack.pop();
                let newVal = {$and: [firstVal, secondVal]};
                valStack.push(newVal);
                topOp = opStack.pop();
            }
            if (topOp === 'OR') opStack.push(topOp);
            opStack.push(element)
        } else { //element is an query object / value
            valStack.push(element);
        }
    });

    while (opStack && opStack.length) {
        const operator = opStack.pop();
        const first = valStack.pop();
        const second = valStack.pop();
        let newVal = (operator === 'OR') ? {$or: [first, second]} : {$and: [first, second]};
        valStack.push(newVal);
    }
    return valStack[0];
}

//helper function for making mongodb query objects from category, value pairs
function constructLogExp(queryParts) {
    let result = [];
    const first = queryParts.slice(0,2);
    result.push(makeRegObj(first[0], first[1]));
    for (let index = 2; index < queryParts.length; index+=3) {
        const operator = queryParts[index];
        const category = queryParts[index + 1];
        const value = queryParts[index + 2];
        result.push(operator);
        result.push(makeRegObj(category, value));
    }
    return result;
}

function makeRegObj(category, value) {
    switch (category) {
        case 'AU':
            return {
                $or: [{ "textAuthor.firstName": new RegExp(value, 'i') },
                    { "textAuthor.lastName": new RegExp(value, 'i') },
                    { "melodyAuthor.firstName": new RegExp(value, 'i') },
                    { "melodyAuthor.lastName": new RegExp(value, 'i') },
                    { "translationAuthor.firstName": new RegExp(value, 'i') },
                    { "translationAuthor.lastName": new RegExp(value, 'i') }
                ]
            };
        case 'TI':
            return {
                $or: [{ "title.estNorth": new RegExp(value, 'i') },
                    { "title.estSouth": new RegExp(value, 'i') },
                    { "title.original": new RegExp(value, 'i') }
                ]
            };
        case 'HI':
            return { "humnInfo": new RegExp(value, 'i') };
        case 'BI':
            return { "bibleReferences": new RegExp(value, 'i') };
        case 'UKLPRC':
            return { "chapterUKLPR": new RegExp(value, 'i') };
        case 'UKLPRSC':
            return { "subchapterUKLPR": new RegExp(value, 'i') };
        case 'UKLPRNR':
            return { "numberUKLPR": new RegExp(value, 'i') };
        case 'ST':
            return { "strroof": value };    
        default:
            return {};
    }
}