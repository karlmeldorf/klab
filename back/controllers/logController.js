const Log = require('../models/log');

exports.getLogs = (req, res, next) => {
    Log
        .find()
        .then(result => {
            console.log(result);
            res.status(200).json({
                logs: result
            });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

exports.postLog = (req, res, next) => {
    const log = new Log()
    Object.assign(log, removeNullsAndEmpty(req.body.query));

    log
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Log saved.',
                log: result
            });
        })
        .catch(err => {
            console.log(err)
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

function removeNullsAndEmpty(obj) {
    var isArray = obj instanceof Array;
    for (var k in obj) {
        if (obj[k] === '' || obj[k] === null) isArray ? obj.splice(k, 1) : delete obj[k];
        else if (typeof obj[k] == "object") removeNullsAndEmpty(obj[k]);
        if (isArray && obj.length == k) removeNullsAndEmpty(obj);
    }

    return obj;
}