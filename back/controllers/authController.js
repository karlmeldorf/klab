const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

exports.postLogin = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    User.findOne({email: email})
        .then(user => {
            if (!user) {
                const error = new Error('User with this email does not exist.');
                error.statusCode = 401;
                throw error;
            }

            bcrypt.compare(password, user.password, function(err, result) {
                if (!result) {
                    const error = new Error('Wrong username or password');
                    error.statusCode = 401;
                    return next(error);
                } else {
                    const token = jwt.sign(
                        {
                        email: user.email,
                        mainAdmin: user.mainAdmin,
                        userId: user._id.toString(),
                        },
                        'karliheasecret',
                        { expiresIn: '24h' }
                    );
                    res.status(200).json({token: token, username: user.username, userId: user._id.toString() });
                }
            });
        })
        .catch(err => {
            const error = new Error('Wrong username or password');
            error.statusCode = 401;
            return next(error);
        })
}