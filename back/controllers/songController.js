const Song = require('../models/song');
const fs = require("fs");
const path = require('path');

exports.postUpdateAuthor = (req, res, next) => {
    // req.body.author = textAuthor || melodyAuthor || translationAuthor
    // req.body.firstName && req.body.lastName && req.body.birthDate && req.body.deathDate
    // req.body.oldFirstName && req.body.oldLastName

    let filterObj = {};
    let setObj = {};

    switch (req.body.query.author) {
        case 'textAuthor':
            filterObj = { $and: [{ "textAuthor.firstName": req.body.query.oldFirstName }, { "textAuthor.lastName": req.body.query.oldLastName }] },
            setObj = { $set: { "textAuthor.firstName": req.body.query.firstName, "textAuthor.lastName": req.body.query.lastName, "textAuthor.birthDate": req.body.query.birthDate, "textAuthor.deathDate": req.body.deathDate }}
            break;
        case 'melodyAuthor':
            filterObj = { $and: [{ "melodyAuthor.firstName": req.body.query.oldFirstName }, { "melodyAuthor.lastName": req.body.query.oldLastName }] },
            setObj = { $set: { "melodyAuthor.firstName": req.body.query.firstName, "melodyAuthor.lastName": req.body.query.lastName, "melodyAuthor.birthDate": req.body.query.birthDate, "melodyAuthor.deathDate": req.body.deathDate }}
            break;
        case 'translationAuthor':
            filterObj = { $and: [{ "translationAuthor.firstName": req.body.query.oldFirstName }, { "translationAuthor.lastName": req.body.query.oldLastName }] },
            setObj = { $set: { "translationAuthor.firstName": req.body.query.firstName, "translationAuthor.lastName": req.body.query.lastName, "translationAuthor.birthDate": req.body.query.birthDate, "translationAuthor.deathDate": req.body.deathDate }}
            break;
    }

    Song
        .updateMany(
            filterObj,
            setObj
        )
        .then(result => {
            res.status(201).json({
                result
            });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
}

exports.getSongs = (req, res, next) => {
    Song
        .find()
        .sort({ $natural: -1 }).limit(40)
        .then(result => {
            console.log(result);
            res.status(200).json({
                songs: result
            });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

exports.getSong = (req, res, next) => {
    const songId = req.params.songId;
    Song.findById(songId)
        .then(song => {
            if (!song) {
                const error = new Error('Could not find a song.');
                error.statusCode = 404;
                throw error;
            }
            res.status(200).json({ message: 'Song fetched.', song: song });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};


function removeNullsAndEmpty(obj) {
    var isArray = obj instanceof Array;
    for (var k in obj) {
        if (obj[k] === '' || obj[k] === null) isArray ? obj.splice(k, 1) : delete obj[k];
        else if (typeof obj[k] == "object") removeNullsAndEmpty(obj[k]);
        if (isArray && obj.length == k) removeNullsAndEmpty(obj);
    }

    return obj;
}

exports.postSong = async (req, res, next) => {
    const song = new Song()
    Object.assign(song, removeNullsAndEmpty(req.body.query));

    song
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Song added!',
                song: result
            });
        })
        .catch(err => {
            console.log(err)
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

exports.postUpdateSong = (req, res, next) => {
    const songId = req.body.query._id;
    const song = new Song()
    Object.assign(song, removeNullsAndEmpty(req.body.query));

    Song
        .findOneAndUpdate({ _id: songId }, song)
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Song updated!',
                song: result
            });
        })
        .catch(err => {
            console.log(err)
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
}

exports.postDeleteSong = (req, res, next) => {
    const songId = req.body.query._id;
    Song
        .findOneAndDelete({ _id: songId })
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Song deleted!',
            });
        })
        .catch(err => {
            console.log(err)
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
}

exports.postUploadFile = (req, res, next) => {
    res.status(201).json({ message: 'success', filePath: req.file.path });
};

exports.postDeleteFile = (req, res, next) => {
    try {
        if (req.body.query.oldpath.length > 1) {
            const oldpath = path.join(__dirname, "..", req.body.query.oldpath);
            if (fs.existsSync(oldpath)) {
                fs.unlinkSync(oldpath);
                res.status(202).json({ message: 'file deleted' });
            }
        } else {
            res.status(404).json({ message: 'failure to update file. File does not exist.' });
        } 
    } catch (err) {
        console.log(err);
    }
};

exports.postUpdateFile = (req, res, next) => {
    try {
        if (req.body.query.oldpath.length > 1) {
            const oldpath = path.join(__dirname, "..", req.body.query.oldpath);
            if (fs.existsSync(oldpath)) {
                fs.unlinkSync(oldpath);
            }
            res.status(201).json({ message: 'success', filePath: req.file.path });
        } else {
            res.status(404).json({ message: 'failure to update file' });
        }
    } catch (err) {
        console.log(err);
    }
}

// PDF või Xml või mp3
exports.getUploadedFile = (req, res, next) => {
    var file = fs.readFileSync(path.join(__dirname, "..", "uploads", req.params.filename));
    res.contentType("application/pdf");
    res.send(file);
};

exports.getUploadedXmlFile = (req, res, next) => {
    var file = fs.readFileSync(path.join(__dirname, "..", "uploads", req.params.filename));
    res.send(file);
};

// MP3
exports.getMp3 = (req, res, next) => {
    var fileName = path.join(__dirname, '..', "uploads", req.params.filename);
    var filestream = fs.createReadStream(fileName);
    var range = req.headers.range.replace("bytes=", "").split('-');

    filestream.on('open', function() {
    var stats = fs.statSync(fileName);
    var fileSizeInBytes = stats["size"];

    // If the start or end of the range is empty, replace with 0 or filesize respectively
    var bytes_start = range[0] ? parseInt(range[0], 10) : 0;
    var bytes_end = range[1] ? parseInt(range[1], 10) : fileSizeInBytes;

    var chunk_size = bytes_end - bytes_start;

    if (chunk_size == fileSizeInBytes) {
        // Serve the whole file as before
        res.writeHead(200, {
        "Accept-Ranges": "bytes",
        'Content-Type': 'audio/mpeg',
        'Content-Length': fileSizeInBytes});
        filestream.pipe(res);
    } else {
        // HTTP/1.1 206 is the partial content response code
        res.writeHead(206, {
        "Content-Range": "bytes " + bytes_start + "-" + bytes_end + "/" + fileSizeInBytes,
        "Accept-Ranges": "bytes",
        'Content-Type': 'audio/mpeg',
        'Content-Length': fileSizeInBytes
        });
        filestream.pipe(res.slice(bytes_start, bytes_end));
    }
    });
};