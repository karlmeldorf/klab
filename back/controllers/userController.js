const User = require('../models/user');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const bcrypt = require('bcryptjs');

exports.registerUser = (req, res, next) => {
    const userId = req.body.query._id;
    const firstname = req.body.query.firstname;
    const lastname = req.body.query.lastname;
    const password = req.body.query.password;

    User
        .find({ "_id": userId })
        .then(result => {
            if (result[0].password || result[0].firstname) {
                res.status(201).json({
                    message: 'Registration code expired!'
                })
            } else {
                bcrypt.hash(password, 5, function(err, hash) {
                    if (err) {
                        console.log(err)
                        if (!err.statusCode) {
                            err.statusCode = 500;
                        }
                        next(err);
                    } else {
                        const newUserData = { firstname: firstname, lastname: lastname, password: hash };
            
                        User
                        .findOneAndUpdate({_id: userId}, newUserData, {new: true})
                        .then(result => {
                            res.status(201).json({
                                message: 'success'
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            if (!err.statusCode) {
                                err.statusCode = 500;
                            }
                            next(err);
                        });
                    }
                });
            }
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}

exports.sendInvitationEmail = (req, res, next) => {
    const recipient = req.body.query.recipient;
    let registrationLink;
    let htmlMessage;

    new User({
        email: recipient,
        mainAdmin: false,
    })
    .save()
    .then(user =>{
        registrationLink = 'https://lauluraamat.eelk.ee/register?registrationCode=' + user._id.toString();
        htmlMessage = '<p>Tere</p><p>Teile on saadetud kutse saada kirikulaulude andmebaasi administraatoriks.'
        + 'Viige registreerimine lõpuni järgmisel lingil: <a href=\''
        + registrationLink + '\'>' + registrationLink + '</a></p>'
        + '<p>Kirikulaulude andmebaas<br><a href=\'https://lauluraamat.eelk.ee\'>https://lauluraamat.eelk.ee</a></p>';
        return htmlMessage
    })
    .then(mail => {
    
        let transporter = nodemailer.createTransport(sendgridTransport({
            auth: {
                api_key: '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'
            }
        }));

        let mailOptions = {
            from: 'kirikulaulu.andmebaas@gmail.com',
            to: recipient,
            subject: 'EELK kirikulaulude andmebaasi teade',
            html: htmlMessage
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
                if (!err.statusCode) {
                    err.statusCode = 500;
                }
                next(err);
            } else {
                res.status(201).json({
                    message: 'Email sent',
                    response: info.response,
                    req: mail
                });
            }
        });
    })
    .catch(err => {
        console.log(err);
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    });
}

exports.changeMainAdminStatus = (req, res, next) => {
    const newValue = { mainAdmin: req.body.query.mainAdmin };
    const userId = req.body.query._id;
    User
        .findOneAndUpdate({_id: userId}, newValue, {new: true})
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'user admin status changed!',
                userData: result
            })
        })
        .catch(err => {
            console.log(err)
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
}



exports.deleteUser = (req, res, next) => {
    const userId = req.body.query._id;
    User
        .findOneAndDelete({ _id: userId})
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'user deleted!',
                userData: result
            });
        })
        .catch(err => {
            console.log(err)
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
}

exports.getUsers = (req, res, next) => {
    User
        .find()
        .then(result => {
            usersNoPwd = []
            result.forEach(user => {
                user.password = '';
                usersNoPwd.push(user);
            });
            res.status(200).json({
                users: usersNoPwd,
                isMainAdmin: req.query.mainAdmin
            });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};